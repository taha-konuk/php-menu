<?php
	require "MenuItem.php";

	# TODO: Set active flag for current menu item by using LinkHelper class.
	# TODO: Ability to lay menu items horizontally.
	class Menu
	{
		# Class name of html element for this menu.
		private $menuClassName = null;

		# Holds menu items of this menu.
		private $items = array();

		public function Menu($className,...$items)
		{
			foreach($items as $item)
			{
//				array_push($this->items,$item);
				$this->addItem($item);
			}
	
			$this->menuClassName = $className;
		}
		
		# Add new menu item.
		public function addItem(MenuItem $item)
		{
			array_push($this->items,$item);
		}
		
		# Remove menu item of index.
		public function removeItem(number $index)
		{
			unset($items[$index]);
		}
		
		# Echoes out the formed html string to show the menu.
		#TODO: Consider sub menu items for every menu item.
		public function show()
		{
			$html = "<ul class={$this->menuClassName}>\n";

			foreach($this->items as $item)
			{
				$className = $item->isSelected() ? MenuItem::$selectedItemClassName : MenuItem::$itemClassName;
				if ($item->getIsCustomHtml())
				{
					$html .= "<li class=".$className.">{$item->customHtml}</li>\n"; 
				} else {
					$html .= "<li class=".$className."><a href={$item->url}>{$item->text}</a></li>\n";
				}
			}

			$html .= "</ul>";

			echo $html;
		}
	};
?>
