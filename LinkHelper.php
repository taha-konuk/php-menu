<?php
class LinkHelper
{

	public static function getCurrentPageLink($isCompleteLink)
	{
		$full_link =  $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		
		if ($isCompleteLink == true)
			return $full_link;
		else
		{
			$param_pos = strpos($full_link,'?');

			if (!empty($param_pos))	//linkin içinde ? var.
				return substr($full_link,0,$param_pos);	
			else
				return $full_link;
		}
	}
};

?>
