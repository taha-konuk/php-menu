<?php
	require "LinkHelper.php";

	/*Represents a menu item in a menu.*/
	class MenuItem
	{
		# Class name of html element for every menu item.
		public static $itemClassName = "nav-item";
		public static $selectedItemClassName = "nav-item-selected";
		
		# Text and url of menu item for classical list. Disabled when using custom html.
		public $text = "";
		public $url = "";
		
		# Custom html string goes between <li></li> tags.
		public $customHtml = "";
		
		# Flag for detecting custom html
		private $isCustomHtml = false;

		# Holds the sub menu items for this menu item if any.
		private $subItems = array();

		public function MenuItem($text,$url)
		{
			$this->text = $text;
			$this->url = $url;
			$this->isCustomHtml = false;
		}

		# Defines the custom html string for this menu item. Sets isCustomHtml flag as true.
		public function customHtml($customHtml)
		{
			$this->customHtml = $customHtml;
			$this->isCustomHtml = true;
		}

		# Add sub menu item for this menu item.
		public function addSubMenuItem(self $menuItem)
		{
			$this->$subItems[] = $menuItem;
		}
		
		# Get readonly value of custom html state.
		public function getIsCustomHtml()
		{
			return $this->isCustomHtml;
		}

		public function isSelected()
		{
			if ($this->isCustomHtml)
				return false;

			$currentLink = LinkHelper::getCurrentPageLink(true);
			
			return $this->url === $currentLink || $this->url === "?".$_SERVER["QUERY_STRING"] ? true : false;
		}
	};
?>
